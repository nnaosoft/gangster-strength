<?php
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('/chat', function () {
    return view('chat');
});

Route::get('/chat-event', function () {
    $message = 'test-message:' . time();
    $user = '123';
//    event(new \App\Events\ChatMessageWasReceived($message, $user));
    broadcast(new \App\Events\ChatMessageWasReceived($message, $user));
});


Route::get('/chat-test', function () {
    return view('chat-test');
});
Route::post('/sendmessage', 'ChatController@sendMessage');

Auth::routes();

Route::get('/home', 'HomeController@index');
