<?php

namespace App\Models;

use App\Models\AppModel;

class Video extends AppModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'videos';

    /**
     * The attributes for validation rules.
     *
     * @var array
     */
    protected $rules = [
        'video_url' => 'required',
    ];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['challengeable_id', 'challengeable_type', 'video_url', 'video_size', 'status'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * Get all of the owning challengeable models.
     */
    public function challengeable()
    {
        return $this->morphTo();
    }
}
