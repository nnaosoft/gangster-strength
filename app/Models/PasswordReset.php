<?php

namespace App\Models;

use App\Models\AppModel as AppModel;

class PasswordReset extends AppModel
{

    /**
     * The attributes for validation rules.
     *
     * @var array
     */
    protected $rules = [
        'email' => 'required|email|unique:password_resets',
    ];
    
}
