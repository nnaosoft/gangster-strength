<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class Socket extends Model
{

    use ValidatingTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sockets';

    /**
     * The attributes for validation rules.
     *
     * @var array
     */
    protected $rules = [
        'socket_id' => 'required|unique:sockets',
    ];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'socket_id', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    public function scopeFindUser($query, $user_id)
    {
        return $query->where('user_id', '=', $user_id);
    }
    
    public function scopeFindSocketID($query, $socket_id)
    {
        return $query->where('socket_id', '=', $socket_id);
    }
}
