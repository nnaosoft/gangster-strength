<?php

namespace App\Models;

use App\Models\AppModel;

class ChallengeReply extends AppModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'challenge_replies';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'challenge_id', 'comment', 'description', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function Challenge()
    {
        return $this->belongsTo('App\Models\Challenge');
    }
    /**
     * Get all of the challenge reply's videos.
     */
//    public function videos()
//    {
//        return $this->morphMany('App\Models\Video', 'challengeable');
//    }

    /**
     * Get all of the challenge reply's video.
     */
    public function video()
    {
        return $this->morphOne('App\Models\Video', 'challengeable');
    }
}
