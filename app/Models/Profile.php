<?php

namespace App\Models;

use App\Models\AppModel;

class Profile extends AppModel
{

    /**
     * The attributes for validation rules.
     *
     * @var array
     */
    protected $rules = [
        'dob' => 'nullable|date',
        'gender' => 'nullable|in:M,F',
        'latitude' => ['nullable', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
        'longitude' => ['nullable', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
        'weight' => 'nullable',
        'height' => 'nullable',
        'biceps' => 'nullable',
        'shoulders' => 'nullable',
        'ethnicity' => 'nullable',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'weight', 'height', 'gender', 'dob', 'biceps', 'shoulders', 'ethnicity', 'gym_name', 'latitude', 'longitude', 'description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'user_id', 'deleted_at', 'updated_at', 'created_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function devices()
    {
        return $this->hasMany('App\Models\Device', 'user_id', 'user_id');
    }
}
