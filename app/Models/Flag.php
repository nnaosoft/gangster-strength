<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

/**
 * Description of Flag
 *
 * @author Samar Haider <s.samar_haider at yahoo.com>
 */
use Illuminate\Database\Eloquent\Model as Eloquent;

class Flag extends Eloquent
{

    protected $table = 'flaggable_flags';
    public $timestamps = true;
    protected $fillable = ['flaggable_id', 'flaggable_type', 'user_id'];

    public function flaggable()
    {
        return $this->morphTo();
    }
}
