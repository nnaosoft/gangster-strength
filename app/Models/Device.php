<?php

namespace App\Models;

use App\Models\AppModel;

class Device extends AppModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'devices';

    /**
     * The attributes for validation rules.
     *
     * @var array
     */
    protected $rules = [
        'device_udid' => 'required',
        'firebase_token' => 'required',
        'user_id' => 'required',
    ];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'device_udid', 'firebase_token', 'created_at', 'updated_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function scopeFindUDID($query, $device_udid)
    {
        return $query->where('device_udid', '=', $device_udid);
    }

    public function scopeFindUser($query, $user_id)
    {
        return $query->where('user_id', '=', $user_id);
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function profile()
    {
        return $this->belongsTo('App\Models\Profile', 'user_id', 'user_id');
    }
}
