<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use App\Models\AppModel;
use Hootlex\Friendships\Traits\Friendable;
use Cog\Ban\Contracts\HasBans as HasBansContract;
use Cog\Ban\Traits\HasBans;
use Gerardojbaez\Messenger\Contracts\MessageableInterface;
use Gerardojbaez\Messenger\Traits\Messageable;

class User extends AppModel implements AuthenticatableContract, HasBansContract, MessageableInterface
{

    use Notifiable,
        Friendable,
        HasBans,
        Messageable,
        Authenticatable;

    const TYPE_ADMIN = 101;
    const TYPE_NORMAL_USER = 1;
    const STATUS_NON_ACTIVE = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_DE_ACTIVE = 3;
    const STATUS_BLOCK = 4;

    /**
     * The attributes for validation rules.
     *
     * @var array
     */
    protected $rules = [
        'email' => 'required|email|unique:users',
        'password' => 'required',
        'username' => 'required|unique:users',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'status', 'password', 'remember_token', 'facebook', 'twitter', 'google', 'user_type', 'deleted_at', 'updated_at',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['password', 'facebook', 'twitter', 'google', 'user_type'];

    /**
     * Attributes that get appended on serialization
     *
     * @var array
     */
    protected $appends = [
//        'defendChallengesCount',
//        'attackChallengesCount',
    ];

    public function providerValidation($provider)
    {
        unset($this->rules['email']);
        unset($this->rules['password']);
        unset($this->rules['username']);
        $this->rules[$provider] = 'required';
        $this->rules['email'] = 'nullable|email|unique:users';
    }

    public function isDeactived()
    {
        return ($this->status == self::STATUS_DE_ACTIVE) ? true : false;
    }

    public function isBlocked()
    {
        return ($this->status == self::STATUS_BLOCK) ? true : false;
    }

    public function profile()
    {
        return $this->hasOne('App\Models\Profile');
    }

    public function devices()
    {
        return $this->hasMany('App\Models\Device');
    }

    public function sockets()
    {
        return $this->hasMany('App\Models\Socket');
    }

    /**
     * Route notifications for the FCM channel.
     *
     * @return string
     */
    public function routeNotificationForFcm()
    {
        return $this->devices()->pluck('firebase_token')->toArray();
    }

    /**
     * Route notifications for the Socket channel.
     *
     * @return string
     */
    public function routeNotificationForSocket()
    {
        return implode($this->sockets()->pluck('socket_id')->toArray());
    }

    public function changePasswordValidation($add = true)
    {
        if ($add) {
            $this->rules = ['password' => 'required'];
        } else {
            unset($this->rules['password']);
        }
    }

    public function events()
    {
        return $this->hasMany('App\Models\Event');
    }

    public function eventParticipants()
    {
        return $this->hasMany('App\Models\EventParticipant');
    }

    public function defendChallenges()
    {
        return $this->hasMany('App\Models\EventParticipant')
                ->findDefend();
    }

    public function getDefendChallengesCountAttribute()
    {
        return $this->defendChallenges()->count();
    }

    public function attackChallenges()
    {
        return $this->hasMany('App\Models\EventParticipant')
                ->findAttack();
    }

    public function getAttackChallengesCountAttribute()
    {
        return $this->attackChallenges()->count();
    }
}
