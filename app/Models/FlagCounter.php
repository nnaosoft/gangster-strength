<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

/**
 * Description of FlagCounter
 *
 * @author Samar Haider <s.samar_haider at yahoo.com>
 */
use Illuminate\Database\Eloquent\Model as Eloquent;

class FlagCounter extends Eloquent
{

    protected $table = 'flaggable_flag_counters';
    public $timestamps = false;
    protected $fillable = ['flaggable_id', 'flaggable_type', 'count'];

    public function flaggable()
    {
        return $this->morphTo();
    }
}
