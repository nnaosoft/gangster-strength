<?php

namespace App\Channels;

use Illuminate\Notifications\Notification;
use Redis;

class SocketChannel
{

    /**
     * The Redis client instance.
     *
     * @var \Redis
     */
    protected $redis;

    /**
     * Create a new Socket channel instance.
     *
     * @param  \Redis  $redis
     * @return void
     */
//    public function __construct(Redis $redis)
//    {
//        $this->redis = $redis;
//    }

    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $message = $this->getData($notifiable, $notification);
        // Send notification to the $notifiable instance...
        if (!$socket_id = $notifiable->routeNotificationFor('socket')) {
            return;
        }
        $message['user'] = $socket_id;
        $socket_channel = $this->getSocketChannel($notification);
        Redis::publish($socket_channel, json_encode($message));
    }

    /**
     * Get the data for the notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return mixed
     *
     * @throws \RuntimeException
     */
    protected function getData($notifiable, Notification $notification)
    {
        if (method_exists($notification, 'toSocket')) {
            return $notification->toBroadcast($notifiable);
        }

        if (method_exists($notification, 'toArray')) {
            return $notification->toArray($notifiable);
        }

        throw new RuntimeException(
        'Socket is missing toArray method.'
        );
    }

    public function getSocketChannel(Notification $notification)
    {
        return config('socket.notification_channel');
    }
}
