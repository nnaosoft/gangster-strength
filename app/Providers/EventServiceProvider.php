<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Log;
use App\Helpers\NotificationHelper;
use App\Notifications\SendFriendRequest;
use App\Notifications\AcceptFriendRequest;

class EventServiceProvider extends ServiceProvider
{

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen('friendships.sent', function ($sender, $recipient) {
            $recipient->notify(new SendFriendRequest($sender));
//            $from = $sender->profile->name;
//            $to = $recipient->profile->name;
//            Log::alert("{$from} sent a friend request to {$to}");
//
//            $tokens = $recipient->devices()->pluck('firebase_token')->toArray();
//            $message = "{$from} sent a friend request to you";
//            $notification = new NotificationHelper();
//            $notification->sendMessage($tokens, $message);
        });
        Event::listen('friendships.accepted', function ($sender, $recipient) {
            $recipient->notify(new AcceptFriendRequest($sender));
//            $from = $sender->profile->name;
//            $to = $recipient->profile->name;
//            Log::alert("{$from} accpet friend request of {$to}");
//
//            $tokens = $recipient->devices()->pluck('firebase_token')->toArray();
//            $message = "{$from} has accpet your friendship request";
//            $notification = new NotificationHelper();
//            $notification->sendMessage($tokens, $message);
        });
        Event::listen('friendships.denied', function ($sender, $recipient) {
            //
        });
        Event::listen('friendships.blocked', function ($sender, $recipient) {
            //
        });
        Event::listen('friendships.cancelled', function ($sender, $recipient) {
            //
        });
    }
}
