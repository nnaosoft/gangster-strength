<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChatMessageWasReceived implements ShouldBroadcast
{

    use Dispatchable,
        InteractsWithSockets,
        SerializesModels;

    public $chatMessage;
    public $user;

    public function __construct($chatMessage, $user)
    {
        $this->chatMessage = $chatMessage;
        $this->user = $user;
        var_dump($this->chatMessage);
//		$data = ['message' => $chatMessage, 'user' => $user];
//        Redis::publish('test-channel', json_encode($data));
    }

    public function broadcastOn()
    {
        return new Channel('chat-room');    # this is channal
    }
    /**
     * The event's broadcast name.
     *
     * @return string
     */
//    public function broadcastAs()
//    {
//        return 'chat-room.1'; # this is listen method
//    }
}
