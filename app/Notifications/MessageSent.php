<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Channels\SocketChannel;

class MessageSent extends Notification
{

    use Queueable;

    public $message;
    public $sender;
    public $reciever;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message, $sender, $reciever)
    {
        $this->message = $message;
        $this->sender = $sender;
        $this->reciever = $reciever;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $via = [];
//        $via = ['mail'];
        if ($notifiable->sockets()->count()) {
            $via[] = SocketChannel::class;
        } elseif ($notifiable->devices()->count()) {
            $via[] = 'fcm';
        }
        return $via;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->line('The introduction to the notification.')
                ->action('Notification Action', url('/'))
                ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'from_user_id' => $this->sender->id,
            'to_user_id' => $notifiable->id,
            'message' => $this->message->body,
        ];
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title' => $this->sender->profile->name . ' has sent you a message.',
            'body' => $this->sender->profile->name . ' has sent you a message.',
            'sound' => '', // Optional 
            'icon' => '', // Optional
            'click_action' => '' // Optional
        ])->data([
            'from_user_id' => $this->sender->id,
            'to_user_id' => $this->reciever->id,
            'message' => $notifiable->body,
        ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.

        return $message;
    }
}
