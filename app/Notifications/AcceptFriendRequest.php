<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AcceptFriendRequest extends Notification
{

    use Queueable;

    public $sender;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($sender)
    {
        $this->sender = $sender;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $via = ['database'];
//        $via = ['mail'];
        if ($notifiable->sockets()->count()) {
//            $via[] = SocketChannel::class;
        } elseif ($notifiable->devices()->count()) {
            $via[] = 'fcm';
        }
        return $via;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->line('The introduction to the notification.')
                ->action('Notification Action', url('/'))
                ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'from_user_id' => $this->sender->id,
            'to_user_id' => $notifiable->id,
            'message' => $this->getMessage(),
        ];
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title' => $this->getMessage(),
            'body' => $this->getMessage(),
            'sound' => '', // Optional 
            'icon' => '', // Optional
            'click_action' => '' // Optional
        ])->data([
            'from_user_id' => $this->sender->id,
            'to_user_id' => $notifiable->id,
        ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.

        return $message;
    }

    public function getMessage()
    {
        return $this->sender->profile->name . ' has accepted your friend request.';
    }
}
