<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use App\Models\Socket;
use Log;

class RedisSubscribe extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:subscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe to a Redis channel';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Redis::subscribe(['socket_disconnect'], function ($socket_info) {
            $socket_info = json_decode($socket_info);
            $socket_id = $socket_info->socket_id;
            $socket = Socket::where('socket_id', '=', $socket_id)->first();
            if ($socket) {
                $socket->delete();
            }
//            Log::info($socket_info);
        });
    }
}
