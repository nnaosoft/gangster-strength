<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Socket;
use App\Helpers\NotificationHelper;

/**
 * @Resource("Sockets", uri="/sockets" )
 */
class SocketController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    /**
     * Register User
     *
     * @Post("/register")
     * 
     * @Parameters({
     *      @Parameter("socket_id", description="Socket ID", required=true)
     * })
     * 
     * @Transaction({
     *      @Request({ "socket_id": "Socket 2"}),
     *      @Response(200, body={"socket":{"socket_id":"Socket 2","user_id":15,"updated_at":"2017-04-27 09:50:33","created_at":"2017-04-27 09:50:33","id":2}}),
     *      @Response(422, body={"message":"Could not register socket.","errors":{"socket_id":{"The socket udid field is required."}},"status_code":422})
     * })
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $socket_id = $request->get('socket_id');
        $socket = Socket::findUser($user->id)->first();
        if ($socket) {
            $socket->delete();
        }
        $socket = new Socket();
        $socket->socket_id = $socket_id;
        $socket->user_id = $user->id;

        if ($socket->isInvalid()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not register socket.', $socket->getErrors());
        }
        $socket->save();
        return $socket;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Delete socket when connection break
     *
     * @Post("/{socket_id}")
     * 
     * @Parameters({
     *      @Parameter("socket_id", description="Socket ID", required=true)
     * })
     * 
     * @Transaction({
     *      @Response(200, body={"socket":{"socket_id":"Socket 2","user_id":15,"updated_at":"2017-04-27 09:50:33","created_at":"2017-04-27 09:50:33","id":2}})
     * })
     */
    public function destroy($id)
    {
        $socket = Socket::findSocketID($id)->first();
        if ($socket) {
            $socket->delete();
        }
        return $socket;
    }
}
