<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;
use Redis;

class ChatController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendMessage()
    {
        $data = ['message' => Request::input('message'), 'user' => Request::input('user')];
        Redis::publish('message', json_encode($data));
        return response()->json([]);

        $data = ['message' => $request->get('message'), 'user' => $request->get('user_id')];
        Redis::publish('message', json_encode($data));
        return $data;
    }
}
