<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

/**
 * @Resource("Notifications", uri="/notifications" )
 */
class NotificationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Auth::user()->notifications;
    }

    public function unread()
    {
        return Auth::user()->unreadNotifications;
    }

    public function read()
    {
        $read_notficiations = Auth::user()
            ->unreadNotifications()
            ->update(['read_at' => Carbon::now()]);
        return [
            'read_count' => $read_notficiations,
        ];
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
//
//    /**
//     * Delete notification
//     *
//     * @Post("/{notification_id}")
//     * 
//     * @Parameters({
//     *      @Parameter("notification_id", description="Notification ID", required=true)
//     * })
//     * 
//     * @Transaction({
//     *      @Response(200, body={"notification":{"notification_id":"Notification 2","user_id":15,"updated_at":"2017-04-27 09:50:33","created_at":"2017-04-27 09:50:33","id":2}})
//     * })
//     */
    public function destroy($id)
    {
        Auth::user()
            ->notifications()
            ->whereKey($id)
            ->delete();
        return [];
    }
}
