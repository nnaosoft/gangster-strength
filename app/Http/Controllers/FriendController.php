<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User as User;
use App\Models\Profile as Profile;

/**
 * @Resource("Friends", uri="/friends" )
 * 
 * Friendship statuses mean
 *  PENDING = 0;
 *  ACCEPTED = 1;
 *  DENIED = 2;
 *  BLOCKED = 3;
 */
class FriendController extends Controller
{

    /**
     * List of friends
     *
     * @Get("/")
     * 
     * @Parameters({
     * })
     * 
     * @Transaction({
     *      @Request({}, headers={"Authorization": "Bearer {token}"}),
     *      @Response(200, body={"total":1,"per_page":20,"current_page":1,"last_page":1,"next_page_url":null,"prev_page_url":null,"from":1,"to":1,"data":{{"id":15,"username":"trowe","email":"delphine52@example.com","created_at":"2017-04-06 06:06:41","profile":{"name":"Dr. Billy White","weight":"65.61","height":"187.31","gender":"M","dob":"1977-09-15","biceps":"28.79","shoulders":"6.87","gym_name":"Vandervort, VonRueden and Krajcik","avatar":"http://lorempixel.com/640/480/?38925","ethnicity":73063,"latitude":"11.45609800","longitude":"-51.78216000","description":"Hic laborum consequatur quibusdam magni laborum iusto quod. Molestias optio molestias enim ducimus repellat suscipit vitae. Dolor non rerum earum et. Et quia et recusandae et ratione rerum."}}}})
     * })
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $user->customEagerLoads(['profile']);
        $friends = $user->getFriends(20);
//        $friends = $user->getAllFriendships();
//        $friends = $user->getPendingFriendships();
//        $friends = $user->getAcceptedFriendships();
//        $friends = $user->getFriendRequests();
        return $friends;
    }

//    /**
//     * List of pending request for friendship
//     * 
//     * Sent by User
//     *
//     * @Get("/request")
//     * 
//     * @Parameters({
//     * })
//     * 
//     * @Transaction({
//     *      @Request({}, headers={"Authorization": "Bearer {token}"}),
//     *      @Response(200, body={"total":1,"per_page":20,"current_page":1,"last_page":1,"next_page_url":null,"prev_page_url":null,"from":1,"to":1,"data":{{"id":15,"username":"trowe","email":"delphine52@example.com","created_at":"2017-04-06 06:06:41","profile":{"name":"Dr. Billy White","weight":"65.61","height":"187.31","gender":"M","dob":"1977-09-15","biceps":"28.79","shoulders":"6.87","gym_name":"Vandervort, VonRueden and Krajcik","avatar":"http://lorempixel.com/640/480/?38925","ethnicity":73063,"latitude":"11.45609800","longitude":"-51.78216000","description":"Hic laborum consequatur quibusdam magni laborum iusto quod. Molestias optio molestias enim ducimus repellat suscipit vitae. Dolor non rerum earum et. Et quia et recusandae et ratione rerum."}}}})
//     * })
//     */
    public function pending(Request $request)
    {
        $user = Auth::user();
        $pending_friends = $user->getPendingFriendships();
        $profiles = [];
        foreach ($pending_friends as $pending_friend) {
            $profiles[] = $pending_friend->recipient;
            $pending_friend->recipient->profile;
        }
        return $profiles;
    }

//
//    /**
//     * List of request for friendship
//     *
//     * Sent to user
//     *
//     * @Get("/request")
//     * 
//     * @Parameters({
//     * })
//     * 
//     * @Transaction({
//     *      @Request({}, headers={"Authorization": "Bearer {token}"}),
//     *      @Response(200, body={"total":1,"per_page":20,"current_page":1,"last_page":1,"next_page_url":null,"prev_page_url":null,"from":1,"to":1,"data":{{"id":15,"username":"trowe","email":"delphine52@example.com","created_at":"2017-04-06 06:06:41","profile":{"name":"Dr. Billy White","weight":"65.61","height":"187.31","gender":"M","dob":"1977-09-15","biceps":"28.79","shoulders":"6.87","gym_name":"Vandervort, VonRueden and Krajcik","avatar":"http://lorempixel.com/640/480/?38925","ethnicity":73063,"latitude":"11.45609800","longitude":"-51.78216000","description":"Hic laborum consequatur quibusdam magni laborum iusto quod. Molestias optio molestias enim ducimus repellat suscipit vitae. Dolor non rerum earum et. Et quia et recusandae et ratione rerum."}}}})
//     * })
//     */
    public function request(Request $request)
    {
        $user = Auth::user();
        $request_friends = $user->getFriendRequests();
        $profiles = [];
        foreach ($request_friends as $request_friend) {
            $profiles[] = $request_friend->sender;
            $request_friend->sender->profile;
        }
        return $profiles;
    }

    public function store(Request $request)
    {
        
    }

    /**
     * Send friend request
     *
     * @Post("/send-request/{id}")
     * 
     * @Parameters({
     * })
     * 
     * @Transaction({
     *      @Request({}, headers={"Authorization": "Bearer {token}"}),
     *      @Response(200, body={}),
     *      @Response(422, body={"message":"Could not send Friend request.","errors":{"user_id":{"Already Send Request"}},"status_code":422})
     * })
     */
    public function send(Request $request, $id)
    {
        $user = Auth::user();
        $recipient = User::FindorFail($id);
        $befreind = $user->befriend($recipient);
        if ($befreind) {
            return $befreind;
        }
        throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not send Friend request.', ['user_id' => 'Request doest not exist.']);
    }
//    public function friend(Request $request)
//    {
////        return $this->store($request, Friend::UPLOADER_CHALLENGER);
//        $friend = new Friend($request->all());
//        $friend->friendrValidation();
//        $friend->user_id = Auth::user()->id;
//        $friend->uploader = Friend::UPLOADER_CHALLENGER;
//
//        if ($friend->isInvalid()) {
//            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not add Friend.', $friend->getErrors());
//        }
//        $friend->save();
//        if ($request->get('tags')) {
//            $friend->retag($request->get('tags'));
//        }
//        return $friend;
//    }

    /**
     * Accept Friend Request
     *
     * @Post("/accept/{id}")
     * 
     * @Parameters({
     * })
     * 
     * @Transaction({
     *      @Response(200, body={}),
     *      @Response(422, body={"message":"Could not accept Friend request.","errors":{"user_id":{"Request doest not exist."}},"status_code":422})
     * })
     */
    public function accept(Request $request, $id)
    {
        $user = Auth::user();
        $sender = User::FindorFail($id);
        if (!$user->hasFriendRequestFrom($sender)) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not accept Friend request.', ['user_id' => 'Request doest not exist.']);
        }
        $accept_request = $user->acceptFriendRequest($sender);
        if ($accept_request) {
            
        }
        return [];
    }

    /**
     * Deny Friend Request
     *
     * @Post("/deny/{id}")
     * 
     * @Transaction({
     *      @Request({}, headers={"Authorization": "Bearer {token}"}),
     *      @Response(200, body={}),
     *      @Response(422, body={"message":"Could not deny Friend request.","errors":{"user_id":{"Request doest not exist."}},"status_code":422})
     * })
     */
    public function deny($id)
    {
        $user = Auth::user();
        $sender = User::FindorFail($id);
        if (!$user->hasFriendRequestFrom($sender)) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not deny Friend request.', ['user_id' => 'Request doest not exist.']);
        }
        $accept_request = $user->denyFriendRequest($sender);
        if ($accept_request) {
            return [];
        }
    }

    /**
     * Remove Friend
     *
     * @Post("/deny/{id}")
     * 
     * @Transaction({
     *      @Request({}, headers={"Authorization": "Bearer {token}"}),
     *      @Response(200, body={}),
     *      @Response(422, body={"message":"Could not remove Friend.","errors":{"user_id":{"Request doest not exist."}},"status_code":422})
     * })
     */
    public function remove(Request $request, $id)
    {
        $user = Auth::user();
        $friend = User::FindorFail($id);
//        if (!$user->isFriendWith($friend)) {
//            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not remove Friend.', ['user_id' => 'Request doest not exist.']);
//        }
        $accept_request = $user->unfriend($friend);
        if ($accept_request) {
            return $accept_request;
        }
    }

    /**
     * Block Friend
     *
     * @Post("/block/{id}")
     * 
     * @Transaction({
     *      @Request({}, headers={"Authorization": "Bearer {token}"}),
     *      @Response(200, body={}),
     *      @Response(422, body={"message":"Could not block Friend.","errors":{"user_id":{"Friend doest not exist."}},"status_code":422})
     * })
     */
    public function block(Request $request, $id)
    {
        $user = Auth::user();
        $friend = User::FindorFail($id);
//        if (!$user->isFriendWith($friend)) {
//            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not block Friend.', ['user_id' => 'Friend doest not exist.']);
//        }
        $accept_request = $user->blockFriend($friend);
        if ($accept_request) {
            return $accept_request;
        }
    }

    /**
     * Unblock Friend Request
     *
     * @Post("/unblock/{id}")
     * 
     * @Transaction({
     *      @Request({}, headers={"Authorization": "Bearer {token}"}),
     *      @Response(200, body={}),
     *      @Response(422, body={"message":"Could not unblock Friend.","errors":{"user_id":{"Friend doest not exist."}},"status_code":422})
     * })
     */
    public function unblock(Request $request, $id)
    {
        $user = Auth::user();
        $sender = User::FindorFail($id);
//        if (!$user->isFriendWith($sender)) {
//            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not unblock Friend.', ['user_id' => 'Friend doest not exist.']);
//        }
        $accept_request = $user->unfriend($sender);
        if ($accept_request) {
            return $accept_request;
        }
    }
}
