@servers(['gs-dev-server' => ['localweb@192.168.0.235'],'gs-live-server' => ['gstrength@gangsterstrength.com']])

@task('listing', ['on' => 'gs-dev-server'])
    cd /var/www/gangster-strength
    ls -la
@endtask

@task('deploy-live', ['on' => 'gs-live-server'])
    cd gs-code/
    php artisan down
    @if ($branch)
        git checkout {{ $branch }}
    @endif
    git pull
    composer install
    php artisan migrate --force
    php artisan cache:clear
    php artisan config:cache
    php artisan route:cache
    php artisan artisan view:clear
    php artisan artisan optimize
    php artisan up
@endtask

@task('deploy-dev-local', ['on' => 'gs-dev-server'])
    cd /var/www/gangster-strength
    php artisan down
    @if ($branch)
        git checkout {{ $branch }}
    @endif
    git pull
    composer install
    php artisan migrate --force
    php artisan up
@endtask

@story('deploy-story', ['on' => 'gs-dev-server'])
    change-dir
    site-down
    git-pull
    composer
    site-up
@endstory

@task('change-dir')
    cd /var/www/gangster-strength
    pwd
@endtask

@task('git-pull')
    @if ($branch)
        git checkout {{ $branch }}
    @endif
        git pull    
@endtask

@task('migrate')
    php artisan migrate
@endtask

@task('composer')
    composer install
@endtask

@task('site-down')
    php artisan down
@endtask

@task('site-up')
    php artisan up
@endtask