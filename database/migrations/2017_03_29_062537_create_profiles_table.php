<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name',100)->nullable()->default(null);
            $table->decimal('weight', 7, 2)->nullable()->default(null);
            $table->decimal('height', 7, 2)->nullable()->default(null);
            $table->enum('gender', ['M', 'F'])->nullable()->default(null);
            $table->date('dob')->nullable()->default(null);
            $table->decimal('biceps', 4, 2)->nullable()->default(null);
            $table->decimal('shoulders', 4, 2)->nullable()->default(null);
            $table->string('gym_name')->nullable()->default(null);
            $table->string('avatar')->nullable()->default(null);
            $table->unsignedMediumInteger('ethnicity')->nullable()->default(null);
            /**
             * Latitude and Longitude
             *  http://stackoverflow.com/questions/12504208/what-mysql-data-type-should-be-used-for-latitude-longitude-with-8-decimal-places
             */
            $table->decimal('latitude', 10, 8)->nullable()->default(null);
            $table->decimal('longitude', 11, 8)->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
//            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->index('user_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
