<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique()->nullable()->default(null);
            $table->string('email')->unique()->nullable()->default(null);
            $table->string('password', 60)->nullable()->default(null);
            $table->tinyInteger('user_type')->default(1);
            $table->unsignedBigInteger('facebook')->nullable()->default(null)->index();
            $table->unsignedBigInteger('twitter')->nullable()->default(null)->index();
            $table->string('google')->nullable()->default(null)->index();
            $table->unsignedTinyInteger('status')->default(1);
            $table->softDeletes()->nullable()->default(null);
//            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
