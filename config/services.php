<?php
return [
    /*
      |--------------------------------------------------------------------------
      | Third Party Services
      |--------------------------------------------------------------------------
      |
      | This file is for storing the credentials for third party services such
      | as Stripe, Mailgun, SparkPost and others. This file provides a sane
      | default location for this type of information, allowing packages
      | to have a conventional place to find your various credentials.
      |
     */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],
    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],
    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],
    'stripe' => [
        'model' => App\Models\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID', '262339607505055'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET', '77f5b4154bbe8a83c06d61fd5ec2f9b1'),
        'redirect' => env('FACEBOOK_REDIRECT', 'http://gangster-strength.local.com/api/users/login/facebook'),
    ],
    'twitter' => [
        'client_id' => env('TWITTER_CLIENT_ID', 'dHWO2u5ZyiYP9KEqYvuSLg'),
        'client_secret' => env('TWITTER_CLIENT_SECRET', 'aP9SGd9rNko4zfjPo972KEnFSfQuwyOu8yCr9FYRCM'),
        'redirect' => env('TWITTER_REDIRECT', 'http://gangster-strength.local.com/api/users/login/twitter'),
    ],
    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID', '629010209379-1r5ffo37t88fuf4c1kfitpc1eqvemk68.apps.googleusercontent.com'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET', 'tcZLo-ghcI4ZMHgr0eJcCHVM'),
        'redirect' => env('GOOGLE_REDIRECT', 'http://gangster-strength.local.com/api/users/login/google'),
    ],
];
