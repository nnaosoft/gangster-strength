<?php
return [
    'message_channel' => env('MESSAGE_CHANNEL', 'message'),
    'notification_channel' => env('NOTIFICATION_CHANNEL', 'message'),
    'log_enabled' => true
];
