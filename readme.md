Install virtual box (https://www.virtualbox.org/wiki/Downloads)

Install vagrant >= 1.8.4 (https://www.vagrantup.com/downloads.html)

Run command 
On OSX and Linux, this is located at /etc/hosts, on Windows it is located at C:\Windows\System32\drivers\etc\hosts.
> sudo gedit /etc/hosts
192.168.56.111  http://gangster-strength.local.com

> vagrant up

> vagrant ssh

> cd /var/www/html/

> composer update

> php artisan migrate

> php artisan db:seed

> composer dump-autoload (if error in above command and rerun last command)

Open browser
- http://gangster-strength.local.com
    Add this line in host
OR
- http://192.168.56.111 (Only to test web server is workring, FB, google, twitter login will create issue)



Change "return $key instanceof $type;" from  "return $annotation instanceof $type;" for creating docs

[View Log file data]
http://gangster-strength.local.com/log-viewer/logs

[Create Models with db fields]
php artisan generate:modelfromtable --table=videos --folder=app/Models --namespace="app\Models"

[Dump users, not run this on production]
php artisan db:seed --class=UsersDumpSeeder



Errors:
Virtual box error “Non existent network interface”
> VBoxManage hostonlyif create

There was an error while executing `VBoxManage`, a CLI used by Vagrant
for controlling VirtualBox. The command and stderr is shown below.

Command: ["modifyvm", "bdce4d10-f530-4183-955f-6fdd9f18afb0", "--natpf1", "delete", "ssh", "--natpf1", "delete", "tcp9199"]

Stderr: VBoxManage: error: The machine 'gangster-strength.local' is already locked for a session (or being unlocked)
VBoxManage: error: Details: code VBOX_E_INVALID_OBJECT_STATE (0x80bb0007), component MachineWrap, interface IMachine, callee nsISupports
VBoxManage: error: Context: "LockMachine(a->session, LockType_Write)" at line 493 of file VBoxManageModifyVM.cpp

>vboxmanage startvm <vm-uuid> --type emergencystop

Run commands in background
> nohup php artisan queue:work --tries=3 > queue.log 2>&1 & echo $! >> save_queue_pid.txt
> nohup php artisan redis:subscribe > redis_subscribe.log 2>&1 & echo $! >> save_redis_subscribe_pid.txt

List of all nohup pids
> jobs -l

For kill process
> kill 19235 (which is in save_queue_pid.txt file)