FORMAT: 1A
HOST: http://192.168.0.235/gangster-strength/api/

# Gangster Strength API

# Users [/users]

## Search users [GET /users]


+ Parameters
    + name: (string, optional) - Search by name or username

+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "total": 1,
                "per_page": 20,
                "current_page": 1,
                "last_page": 1,
                "next_page_url": null,
                "prev_page_url": null,
                "from": 1,
                "to": 1,
                "data": [
                    {
                        "id": 27,
                        "username": "wilmer18",
                        "email": "jamaal23@example.org",
                        "created_at": "2017-04-06 06:10:33",
                        "profile": {
                            "name": "Dorris Jakubowski IV",
                            "weight": "109.21",
                            "height": "176.66",
                            "gender": "M",
                            "dob": "1993-08-28",
                            "biceps": "8.62",
                            "shoulders": "45.91",
                            "gym_name": "Adams-Smith",
                            "avatar": "uploads\/avatars\/M6pYujXszsU3axr6X4IvO73ZrrEF9S18BWPxqsCy.jpeg",
                            "ethnicity": 207623,
                            "latitude": "74.51276300",
                            "longitude": "-154.56846400",
                            "description": "WILL do next! As for pulling me out of the leaves: 'I should think you'll feel it a minute or two, she made out what it meant till now.' 'If that's all you know what they're like.' 'I believe so,'."
                        }
                    }
                ]
            }

## Register User [POST /users/register]


+ Parameters
    + username: (string, required) - Username should be unique
    + email: (email, required) - Email should be unique
    + password: (string, required) - User Password
    + name: (string, optional) - Full Name of user

+ Request (application/json)
    + Body

            {
                "username": "user2",
                "email": "user2@mailinator.com",
                "password": "123456",
                "name": "User Two"
            }

+ Response 200 (application/json)
    + Body

            {
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIsImlzcyI6Imh0dHA6XC9cL2dhbmdzdGVyLXN0cmVuZ3RoLmxvY2FsLmNvbVwvYXBpXC91c2Vyc1wvcmVnaXN0ZXIiLCJpYXQiOjE0OTEzMDQ4NjQsImV4cCI6MTQ5MTMwODQ2NCwibmJmIjoxNDkxMzA0ODY0LCJqdGkiOiIxYzA0ZDMxNDk5NWQ5ZWJlYWE1Yzk0MTdkYzQ5MzA4NiJ9.R2OMlFOwPzqtrgIgoZpF9VAH1Zm0BnLYMt2PzTR8LUk",
                "user": {
                    "username": "user2",
                    "email": "user2@mailinator.com",
                    "created_at": "2017-04-04 11:21:03",
                    "id": 2,
                    "profile": {
                        "name": "User Two",
                        "weight": null,
                        "height": null,
                        "gender": null,
                        "dob": null,
                        "biceps": null,
                        "shoulders": null,
                        "gym_name": null,
                        "avatar": null,
                        "ethnicity": null,
                        "latitude": null,
                        "longitude": null,
                        "description": null
                    }
                }
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not add new user.",
                "errors": {
                    "email": [
                        "The email field is required."
                    ],
                    "username": [
                        "The username field is required."
                    ],
                    "password": [
                        "The password field is required."
                    ]
                },
                "status_code": 422
            }

## Login user [POST /users/login]
Login user with a `email` and `password`.
Token is returned which will be required in every request

+ Request (application/json)
    + Body

            {
                "email": "user1@mailinator.com",
                "password": "123456"
            }

+ Response 200 (application/json)
    + Body

            {
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIsImlzcyI6Imh0dHA6XC9cL2dhbmdzdGVyLXN0cmVuZ3RoLmxvY2FsXC9hcGlcL3VzZXJzXC9yZWdpc3RlciIsImlhdCI6MTQ5MTIwNDU4MSwiZXhwIjoxNDkxMjA4MTgxLCJuYmYiOjE0OTEyMDQ1ODEsImp0aSI6ImZiMzAxMzI1YzgyMmRiMzkxMzhmOTkzMjc0MDQ5NTk1In0.L2PcdY3kuUdakNzgWirglwuJqCTtdLa-uHaAfL5OZqA",
                "user": {
                    "username": "user2",
                    "email": "user2@mailinator.com",
                    "created_at": "2017-04-03 07:29:40",
                    "id": 2,
                    "profile": {
                        "name": "User Two",
                        "weight": null,
                        "height": null,
                        "gender": null,
                        "dob": null,
                        "biceps": null,
                        "shoulders": null,
                        "gym_name": null,
                        "avatar": null,
                        "ethnicity": null,
                        "latitude": null,
                        "longitude": null,
                        "description": null
                    }
                }
            }

+ Response 401 (application/json)
    + Body

            {
                "error": "invalid_credentials",
                "message": "Invalid credentials",
                "status_code": 401
            }

+ Response 401 (application/json)
    + Body

            {
                "error": "user_deactivated",
                "message": "Your Account has been deactivated. Please email us at abc@xyz.com to reactivate your account.",
                "status_code": 401
            }

+ Response 500 (application/json)
    + Body

            {
                "error": "could_not_create_token",
                "message": "Internal Server Error",
                "status_code": 500
            }

## Login with Google [POST /users/login/google]
Login user with a google code.
Token is returned which will be required in every request

+ Request (application/json)
    + Body

            {
                "code": "4/7zE1BAw89p1hyBuVS1NCMjMVIVfHD81VIPo0PdFhpTU"
            }

+ Response 200 (application/json)
    + Body

            {
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIsImlzcyI6Imh0dHA6XC9cL2dhbmdzdGVyLXN0cmVuZ3RoLmxvY2FsXC9hcGlcL3VzZXJzXC9yZWdpc3RlciIsImlhdCI6MTQ5MTIwNDU4MSwiZXhwIjoxNDkxMjA4MTgxLCJuYmYiOjE0OTEyMDQ1ODEsImp0aSI6ImZiMzAxMzI1YzgyMmRiMzkxMzhmOTkzMjc0MDQ5NTk1In0.L2PcdY3kuUdakNzgWirglwuJqCTtdLa-uHaAfL5OZqA",
                "user": {
                    "username": "user2",
                    "email": "user2@mailinator.com",
                    "created_at": "2017-04-03 07:29:40",
                    "id": 2,
                    "profile": {
                        "name": "User Two",
                        "weight": null,
                        "height": null,
                        "gender": null,
                        "dob": null,
                        "biceps": null,
                        "shoulders": null,
                        "gym_name": null,
                        "avatar": null,
                        "ethnicity": null,
                        "latitude": null,
                        "longitude": null,
                        "description": null
                    }
                }
            }

+ Response 401 (application/json)
    + Body

            {
                "error": "invalid_credentials",
                "message": "Invalid credentials",
                "status_code": 401
            }

+ Response 401 (application/json)
    + Body

            {
                "error": "user_deactivated",
                "message": "Your Account has been deactivated. Please email us at abc@xyz.com to reactivate your account.",
                "status_code": 401
            }

+ Response 500 (application/json)
    + Body

            {
                "error": "could_not_create_token",
                "message": "Internal Server Error",
                "status_code": 500
            }

## Login with Facebook [POST /users/login/facebook]
Login user with a facebook code.
Token is returned which will be required in every request

+ Request (application/json)
    + Body

            {
                "code": "AQDB5WWoJsQCgg4mvJaczTY8ZKvUpDMemUwJf9fP3r44wXJtTaM2I5mYK43Dx3DIf5_M4RH_2lGybuavQJ6uRT2tiLPkjTYguVYYylx1G-ZPtW88aiFpz3D3126-THki87OEFnqwQQDCPhrbc7yDYgwNS5ld3aU4Kx44ruwtjKlB2v6qdgpuZcF6A4E0t6Vt5ua_tUGYB7YDFpXCsCNyXtDVRpPDxrUyf0iX3lrGax6l4Qdj_1zY4akm4DgrUgUmXcnjYoR1jf3uKVRCwm-qWXbbSLTdSsmSgc4sq8bd1ywChwYopEWFkhXikUn2civT63Gk5gq3ueBEA1y-TOijKrj8#_=_"
            }

+ Response 200 (application/json)
    + Body

            {
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIsImlzcyI6Imh0dHA6XC9cL2dhbmdzdGVyLXN0cmVuZ3RoLmxvY2FsXC9hcGlcL3VzZXJzXC9yZWdpc3RlciIsImlhdCI6MTQ5MTIwNDU4MSwiZXhwIjoxNDkxMjA4MTgxLCJuYmYiOjE0OTEyMDQ1ODEsImp0aSI6ImZiMzAxMzI1YzgyMmRiMzkxMzhmOTkzMjc0MDQ5NTk1In0.L2PcdY3kuUdakNzgWirglwuJqCTtdLa-uHaAfL5OZqA",
                "user": {
                    "username": "user2",
                    "email": "user2@mailinator.com",
                    "created_at": "2017-04-03 07:29:40",
                    "id": 2,
                    "profile": {
                        "name": "User Two",
                        "weight": null,
                        "height": null,
                        "gender": null,
                        "dob": null,
                        "biceps": null,
                        "shoulders": null,
                        "gym_name": null,
                        "avatar": null,
                        "ethnicity": null,
                        "latitude": null,
                        "longitude": null,
                        "description": null
                    }
                }
            }

+ Response 401 (application/json)
    + Body

            {
                "error": "invalid_credentials",
                "message": "Invalid credentials",
                "status_code": 401
            }

+ Response 401 (application/json)
    + Body

            {
                "error": "user_deactivated",
                "message": "Your Account has been deactivated. Please email us at abc@xyz.com to reactivate your account.",
                "status_code": 401
            }

+ Response 500 (application/json)
    + Body

            {
                "error": "could_not_create_token",
                "message": "Internal Server Error",
                "status_code": 500
            }

## Login with Twitter [POST /users/login/twitter]
First request has no need input data which response has oauth_token, oauth_token_secret & oauth_callback_confirmed
Second request has need oauth_token, oauth_verifier  input data which response has token & user profile
Token is returned which will be required in every request

+ Request (application/json)
    + Body

            []

+ Request (application/json)
    + Body

            {
                "oauth_token": "3X2JvwAAAAAAz5owAAABW0LVcCc",
                "oauth_verifier": "xWh0HnOvP0ffk8riAnto7SVwElxFDBJl"
            }

+ Response 200 (application/json)
    + Body

            {
                "oauth_token": "9Aw2gwAAAAAAz5owAAABW0L4mQc",
                "oauth_token_secret": "Bdknx2I6qeEU372OkV0iKbARur4hNbli",
                "oauth_callback_confirmed": "true"
            }

+ Response 200 (application/json)
    + Body

            {
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIsImlzcyI6Imh0dHA6XC9cL2dhbmdzdGVyLXN0cmVuZ3RoLmxvY2FsXC9hcGlcL3VzZXJzXC9yZWdpc3RlciIsImlhdCI6MTQ5MTIwNDU4MSwiZXhwIjoxNDkxMjA4MTgxLCJuYmYiOjE0OTEyMDQ1ODEsImp0aSI6ImZiMzAxMzI1YzgyMmRiMzkxMzhmOTkzMjc0MDQ5NTk1In0.L2PcdY3kuUdakNzgWirglwuJqCTtdLa-uHaAfL5OZqA",
                "user": {
                    "username": "user2",
                    "email": "user2@mailinator.com",
                    "created_at": "2017-04-03 07:29:40",
                    "id": 2,
                    "profile": {
                        "name": "User Two",
                        "weight": null,
                        "height": null,
                        "gender": null,
                        "dob": null,
                        "biceps": null,
                        "shoulders": null,
                        "gym_name": null,
                        "avatar": null,
                        "ethnicity": null,
                        "latitude": null,
                        "longitude": null,
                        "description": null
                    }
                }
            }

+ Response 401 (application/json)
    + Body

            {
                "error": "invalid_credentials",
                "message": "Invalid credentials",
                "status_code": 401
            }

+ Response 401 (application/json)
    + Body

            {
                "error": "user_deactivated",
                "message": "Your Account has been deactivated. Please email us at abc@xyz.com to reactivate your account.",
                "status_code": 401
            }

+ Response 500 (application/json)
    + Body

            {
                "error": "could_not_create_token",
                "message": "Internal Server Error",
                "status_code": 500
            }

## Show User Profile [GET /users/{id}]


+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "user": {
                    "id": 13,
                    "username": "thilpert",
                    "email": "lorena.rohan@example.net",
                    "created_at": "2017-05-09 10:05:19",
                    "banned_at": null,
                    "friendship": null,
                    "defendChallengesCount": 1,
                    "attackChallengesCount": 0,
                    "participants": [
                        {
                            "id": 4,
                            "event_id": "2",
                            "user_id": "13",
                            "video_url": "http:\/\/gangster-strength.local.com",
                            "status": "1",
                            "uploader": "2",
                            "comment": null,
                            "deleted_at": null,
                            "created_at": "2017-05-15 10:51:46",
                            "updated_at": "2017-05-15 10:51:46",
                            "profile": {
                                "name": "Pedro Romaguera",
                                "weight": "166.03",
                                "height": "194.54",
                                "gender": "M",
                                "dob": "1942-07-19",
                                "biceps": "33.05",
                                "shoulders": "47.84",
                                "gym_name": "Gorczany Ltd",
                                "avatar": "http:\/\/lorempixel.com\/640\/480\/?36148",
                                "ethnicity": "9",
                                "latitude": "41.89955600",
                                "longitude": "-135.45465100",
                                "description": "Queen of Hearts, she made some tarts, All on a little bottle on it, ('which certainly was not much larger than a rat-hole: she knelt down and looked along the sea-shore--' 'Two lines!' cried the."
                            }
                        }
                    ],
                    "profile": {
                        "name": "Pedro Romaguera",
                        "weight": "166.03",
                        "height": "194.54",
                        "gender": "M",
                        "dob": "1942-07-19",
                        "biceps": "33.05",
                        "shoulders": "47.84",
                        "gym_name": "Gorczany Ltd",
                        "avatar": "http:\/\/lorempixel.com\/640\/480\/?36148",
                        "ethnicity": "9",
                        "latitude": "41.89955600",
                        "longitude": "-135.45465100",
                        "description": "Queen of Hearts, she made some tarts, All on a little bottle on it, ('which certainly was not much larger than a rat-hole: she knelt down and looked along the sea-shore--' 'Two lines!' cried the."
                    }
                }
            }

+ Response 404 (application/json)
    + Body

            {
                "message": "No query results for model [App\\Models\\User] 123",
                "status_code": 404
            }

## Show My Account info [GET /users/me]


+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "user": {
                    "id": 4,
                    "username": "jamaal23",
                    "email": "jamaal23@example.org",
                    "created_at": "2017-05-03 07:25:41",
                    "banned_at": null,
                    "defendChallengesCount": 0,
                    "attackChallengesCount": 2,
                    "participants": [
                        {
                            "id": 2,
                            "event_id": "1",
                            "user_id": "6",
                            "video_url": "http:\/\/gangster-strength.local.com",
                            "status": "1",
                            "uploader": "2",
                            "comment": null,
                            "deleted_at": null,
                            "created_at": "2017-05-12 11:45:57",
                            "updated_at": "2017-05-12 11:45:57",
                            "profile": {
                                "name": "Jillian Russel",
                                "weight": "78.33",
                                "height": "167.03",
                                "gender": "M",
                                "dob": "1958-10-13",
                                "biceps": "24.57",
                                "shoulders": "23.58",
                                "gym_name": "Gibson, Altenwerth and Schmitt",
                                "avatar": "http:\/\/lorempixel.com\/640\/480\/?86818",
                                "ethnicity": "5",
                                "latitude": "-74.78137600",
                                "longitude": "96.60966900",
                                "description": "I have dropped them, I wonder?' And here Alice began in a trembling voice, 'Let us get to the other, and growing sometimes taller and sometimes shorter, until she had found the fan and gloves. 'How."
                            }
                        },
                        {
                            "id": 4,
                            "event_id": "2",
                            "user_id": "13",
                            "video_url": "http:\/\/gangster-strength.local.com",
                            "status": "1",
                            "uploader": "2",
                            "comment": null,
                            "deleted_at": null,
                            "created_at": "2017-05-15 10:51:46",
                            "updated_at": "2017-05-15 10:51:46",
                            "profile": {
                                "name": "Pedro Romaguera",
                                "weight": "166.03",
                                "height": "194.54",
                                "gender": "M",
                                "dob": "1942-07-19",
                                "biceps": "33.05",
                                "shoulders": "47.84",
                                "gym_name": "Gorczany Ltd",
                                "avatar": "http:\/\/lorempixel.com\/640\/480\/?36148",
                                "ethnicity": "9",
                                "latitude": "41.89955600",
                                "longitude": "-135.45465100",
                                "description": "Queen of Hearts, she made some tarts, All on a little bottle on it, ('which certainly was not much larger than a rat-hole: she knelt down and looked along the sea-shore--' 'Two lines!' cried the."
                            }
                        }
                    ],
                    "profile": {
                        "name": "jamaal 23",
                        "weight": null,
                        "height": null,
                        "gender": null,
                        "dob": null,
                        "biceps": null,
                        "shoulders": null,
                        "gym_name": null,
                        "avatar": null,
                        "ethnicity": null,
                        "latitude": null,
                        "longitude": null,
                        "description": null
                    }
                }
            }

## Update My Profile Information [POST /users/me]


+ Parameters
    + name: (string, optional) - Customer Name
    + weight: (decimal, optional) - 
    + height: (decimal, optional) - 
    + gender: (string, optional) - Gender is M/F
    + dob: (date, optional) - format is Y-m-d like 1985-12-12
    + biceps: (decimal, optional) - 
    + shoulders: (decimal, optional) - 
    + gym_name: (string, optional) - 
    + ethnicity: (integer, optional) - 
    + latitude: (decimal, optional) - 
    + longitude: (decimal, optional) - 
    + description: (string, optional) - 

+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            {
                "name": "User One",
                "weight": 111.6,
                "height": 169.6,
                "gender": "M",
                "dob": "1989-05-27",
                "biceps": 13.4,
                "shoulders": 16.5,
                "gym_name": "Best Gym",
                "avatar": null,
                "ethnicity": null,
                "latitude": null,
                "longitude": null,
                "description": "I am professioal Builder"
            }

+ Response 200 (application/json)
    + Body

            {
                "user": {
                    "id": 1,
                    "username": null,
                    "email": null,
                    "created_at": "2017-04-04 11:19:11",
                    "profile": {
                        "name": null,
                        "weight": 111.6,
                        "height": 169.6,
                        "gender": "M",
                        "dob": "1989-05-27",
                        "biceps": null,
                        "shoulders": null,
                        "gym_name": "Best Gym",
                        "avatar": null,
                        "ethnicity": null,
                        "latitude": null,
                        "longitude": null,
                        "description": "I am professional Builder"
                    }
                }
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not update user profile information.",
                "errors": {
                    "dob": [
                        "The dob is not a valid date."
                    ],
                    "gender": [
                        "The selected gender is invalid."
                    ]
                },
                "status_code": 422
            }

## Videos List uploaded by user [GET /users/videos/{id}]


+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "total": 2,
                "per_page": 10,
                "current_page": 1,
                "last_page": 1,
                "next_page_url": null,
                "prev_page_url": null,
                "from": 1,
                "to": 2,
                "data": [
                    {
                        "id": 3,
                        "event_id": "2",
                        "user_id": "4",
                        "video_url": "http:\/\/gangster-strength.local.com",
                        "status": "1",
                        "uploader": "1",
                        "comment": null,
                        "deleted_at": null,
                        "created_at": "2017-05-15 10:50:12",
                        "updated_at": "2017-05-15 10:50:12"
                    },
                    {
                        "id": 1,
                        "event_id": "1",
                        "user_id": "4",
                        "video_url": "http:\/\/gangster-strength.local.com",
                        "status": "1",
                        "uploader": "1",
                        "comment": null,
                        "deleted_at": null,
                        "created_at": "2017-05-12 11:45:30",
                        "updated_at": "2017-05-12 11:45:30"
                    }
                ]
            }

## Send Password Reset Code [POST /users/send-password-reset-code]


+ Parameters
    + email: (string, required) - Email for code

+ Request (application/json)
    + Body

            {
                "email": "user@mailinator.om"
            }

+ Response 200 (application/json)

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not send reset password email.",
                "errors": {
                    "email": [
                        "Email does not exists."
                    ]
                },
                "status_code": 422
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not send reset password email.",
                "status_code": 422
            }

## Reset Customer password [POST /users/reset-password]


+ Parameters
    + email: (string, required) - Email for code
    + token: (string, required) - 4 digits code
    + password: (string, required) - 4 digits password

+ Request (application/json)
    + Body

            {
                "email": "user1@mailinator.com",
                "token": 3646,
                "password": 1234,
                "confirm_password": 1234
            }

+ Response 200 (application/json)
    + Body

            {
                "user": {
                    "email": "user1@mailinator.com",
                    "name": "Customer One",
                    "plate_number": "KBP-2440",
                    "telephone_number": 123456789,
                    "user_type": 3,
                    "updated_at": "2016-12-13 08:15:30",
                    "created_at": "2016-12-01 06:16:52",
                    "confirm_password": 1234,
                    "id": "583fc0547d2ae705f534d4b1"
                }
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not update user password.",
                "errors": {
                    "email": [
                        "Email does not exists."
                    ]
                },
                "status_code": 422
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not update user password.",
                "errors": {
                    "token": [
                        "Code does not match."
                    ]
                },
                "status_code": 422
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not update user password.",
                "errors": {
                    "password": [
                        "The password field is required."
                    ]
                },
                "status_code": 422
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not send reset password email.",
                "status_code": 422
            }

## Upload User Avatar [POST /users/avatar]


+ Parameters
    + image: (string, required) - This request is not Json based. So, please be careful before using it

+ Response 200 (application/json)
    + Body

            {
                "profile": {
                    "name": "Dorris Jakubowski IV",
                    "weight": "109.21",
                    "height": "176.66",
                    "gender": "M",
                    "dob": "1993-08-28",
                    "biceps": "8.62",
                    "shoulders": "45.91",
                    "gym_name": "Adams-Smith",
                    "avatar": "uploads\/avatars\/M6pYujXszsU3axr6X4IvO73ZrrEF9S18BWPxqsCy.jpeg",
                    "ethnicity": 207623,
                    "latitude": "74.51276300",
                    "longitude": "-154.56846400",
                    "description": "WILL do next! As for pulling me out of the leaves: 'I should think you'll feel it a minute or two, she made out what it meant till now.' 'If that's all you know what they're like.' 'I believe so,'."
                }
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not update user avatar.",
                "errors": {
                    "errors": {
                        "avatar": "The avatar field is required."
                    }
                },
                "status_code": 422
            }

# Challenges [/challenges]

## List of challenges [GET /challenges]


+ Parameters
    + title: (string, optional) - Search by title of challenge 
    + status: (integer, optional) - 1= new challenges , 3= completed challenges, default completed listing show
    + type: (integer, optional) - 1= REPLY, 2 = LIVE, if leave it both result return
    + user_id: (integer, optional) - Get user completed or new challenges
    + tags: (array, optional) - Search by tags of challenge

+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "total": 1,
                "per_page": 20,
                "current_page": 1,
                "last_page": 1,
                "next_page_url": null,
                "prev_page_url": null,
                "from": 1,
                "to": 1,
                "data": [
                    {
                        "id": 2,
                        "title": "Challenge Two",
                        "type": 1,
                        "status": 3,
                        "user_id": 27,
                        "description": "I am professioal Builder",
                        "created_at": "2017-04-18 12:58:42",
                        "tags": [
                            "100",
                            "Pushups"
                        ],
                        "likes": 2,
                        "liked": true,
                        "flagged": false,
                        "challenger_video": {
                            "id": 2,
                            "event_id": 2,
                            "user_id": 27,
                            "video_url": "http:\/\/gangster-strength.local.com",
                            "status": 1,
                            "uploader": 1,
                            "comment": null,
                            "deleted_at": null,
                            "created_at": "2017-04-18 12:58:43",
                            "updated_at": "2017-04-18 12:58:43",
                            "profile": {
                                "name": "Dorris Jakubowski IV",
                                "weight": "109.21",
                                "height": "176.66",
                                "gender": "M",
                                "dob": "1993-08-28",
                                "biceps": "8.62",
                                "shoulders": "45.91",
                                "gym_name": "Adams-Smith",
                                "avatar": "uploads\/avatars\/M6pYujXszsU3axr6X4IvO73ZrrEF9S18BWPxqsCy.jpeg",
                                "ethnicity": 207623,
                                "latitude": "74.51276300",
                                "longitude": "-154.56846400",
                                "description": "WILL do next! As for pulling me out of the leaves: 'I should think you'll feel it a minute or two, she made out what it meant till now.' 'If that's all you know what they're like.' 'I believe so,'."
                            }
                        },
                        "defender_video": {
                            "id": 4,
                            "event_id": 2,
                            "user_id": 27,
                            "video_url": "http:\/\/gangster-strength.local.com",
                            "status": 1,
                            "uploader": 2,
                            "comment": null,
                            "deleted_at": null,
                            "created_at": "2017-04-19 07:15:18",
                            "updated_at": "2017-04-19 07:15:18",
                            "profile": {
                                "name": "Dorris Jakubowski IV",
                                "weight": "109.21",
                                "height": "176.66",
                                "gender": "M",
                                "dob": "1993-08-28",
                                "biceps": "8.62",
                                "shoulders": "45.91",
                                "gym_name": "Adams-Smith",
                                "avatar": "uploads\/avatars\/M6pYujXszsU3axr6X4IvO73ZrrEF9S18BWPxqsCy.jpeg",
                                "ethnicity": 207623,
                                "latitude": "74.51276300",
                                "longitude": "-154.56846400",
                                "description": "WILL do next! As for pulling me out of the leaves: 'I should think you'll feel it a minute or two, she made out what it meant till now.' 'If that's all you know what they're like.' 'I believe so,'."
                            }
                        }
                    }
                ]
            }

## Friends Challenges [GET /challenges/timeline]


+ Parameters
    + status: (integer, optional) - 1= new challenges , 3= completed challenges, default completed listing show

+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "total": 1,
                "per_page": 20,
                "current_page": 1,
                "last_page": 1,
                "next_page_url": null,
                "prev_page_url": null,
                "from": 1,
                "to": 1,
                "data": [
                    {
                        "id": 2,
                        "title": "Challenge Two",
                        "type": 1,
                        "status": 3,
                        "user_id": 27,
                        "description": "I am professioal Builder",
                        "created_at": "2017-04-18 12:58:42",
                        "tags": [
                            "100",
                            "Pushups"
                        ],
                        "likes": 2,
                        "liked": true,
                        "flagged": false,
                        "challenger_video": {
                            "id": 2,
                            "event_id": 2,
                            "user_id": 27,
                            "video_url": "http:\/\/gangster-strength.local.com",
                            "status": 1,
                            "uploader": 1,
                            "comment": null,
                            "deleted_at": null,
                            "created_at": "2017-04-18 12:58:43",
                            "updated_at": "2017-04-18 12:58:43",
                            "profile": {
                                "name": "Dorris Jakubowski IV",
                                "weight": "109.21",
                                "height": "176.66",
                                "gender": "M",
                                "dob": "1993-08-28",
                                "biceps": "8.62",
                                "shoulders": "45.91",
                                "gym_name": "Adams-Smith",
                                "avatar": "uploads\/avatars\/M6pYujXszsU3axr6X4IvO73ZrrEF9S18BWPxqsCy.jpeg",
                                "ethnicity": 207623,
                                "latitude": "74.51276300",
                                "longitude": "-154.56846400",
                                "description": "WILL do next! As for pulling me out of the leaves: 'I should think you'll feel it a minute or two, she made out what it meant till now.' 'If that's all you know what they're like.' 'I believe so,'."
                            }
                        },
                        "defender_video": {
                            "id": 4,
                            "event_id": 2,
                            "user_id": 27,
                            "video_url": "http:\/\/gangster-strength.local.com",
                            "status": 1,
                            "uploader": 2,
                            "comment": null,
                            "deleted_at": null,
                            "created_at": "2017-04-19 07:15:18",
                            "updated_at": "2017-04-19 07:15:18",
                            "profile": {
                                "name": "Dorris Jakubowski IV",
                                "weight": "109.21",
                                "height": "176.66",
                                "gender": "M",
                                "dob": "1993-08-28",
                                "biceps": "8.62",
                                "shoulders": "45.91",
                                "gym_name": "Adams-Smith",
                                "avatar": "uploads\/avatars\/M6pYujXszsU3axr6X4IvO73ZrrEF9S18BWPxqsCy.jpeg",
                                "ethnicity": 207623,
                                "latitude": "74.51276300",
                                "longitude": "-154.56846400",
                                "description": "WILL do next! As for pulling me out of the leaves: 'I should think you'll feel it a minute or two, she made out what it meant till now.' 'If that's all you know what they're like.' 'I believe so,'."
                            }
                        }
                    }
                ]
            }

## Create challenge [POST /challenges]


+ Parameters
    + title: (string, required) - Challenge title
    + type: (integer, required) - 1= REPLY, 2 = LIVE
    + description: (string, optional) - Challenge description
    + tags: (array, optional) - tags of challenge
    + video_url: (string, required) - Videos url on AWS S3 required only for reply challenge

+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            {
                "title": "User One",
                "type": 1,
                "description": "I am professioal Builder",
                "video_url": "http://gangster-strength.local.com",
                "tags": [
                    "pushups",
                    "100"
                ]
            }

+ Response 200 (application/json)
    + Body

            {
                "challenge": {
                    "title": "User One",
                    "type": 1,
                    "description": "I am professioal Builder",
                    "video_url": "http:\/\/gangster-strength.local.com",
                    "user_id": 27,
                    "uploader": 1,
                    "created_at": "2017-04-13 10:24:32",
                    "id": 2,
                    "tags": [
                        "Pushups",
                        "100"
                    ]
                }
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not add Challenge.",
                "errors": {
                    "video_url": [
                        "The video url field is required."
                    ]
                },
                "status_code": 422
            }

## Defend challenge [POST /challenges/defend]


+ Parameters
    + event_id: (integer, required) - Challenge Id
    + video_url: (string, required) - Videos url on AWS S3

+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            {
                "event_id": 1,
                "video_url": "http://gangster-strength.local.com"
            }

+ Response 200 (application/json)
    + Body

            {
                "event": {
                    "id": 1,
                    "title": "Challenge One",
                    "type": 1,
                    "status": 3,
                    "user_id": 27,
                    "description": "I am professioal Builder",
                    "created_at": "2017-04-18 12:58:37",
                    "tags": [
                        "100",
                        "Pushups"
                    ]
                }
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not reply on Challenge.",
                "errors": {
                    "event_id": [
                        "Invalid Event Id"
                    ]
                },
                "status_code": 422
            }

## Show Challenge Details [GET /challenges/{id}]


+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "event": {
                    "id": 2,
                    "title": "Challenge Two",
                    "type": 1,
                    "status": 3,
                    "user_id": 27,
                    "description": "I am professioal Builder",
                    "created_at": "2017-04-18 12:58:42",
                    "tags": [
                        "100",
                        "Pushups"
                    ],
                    "likes": 2,
                    "liked": true,
                    "flagged": false,
                    "challenger_video": {
                        "id": 2,
                        "event_id": 2,
                        "user_id": 27,
                        "video_url": "http:\/\/gangster-strength.local.com",
                        "status": 1,
                        "uploader": 1,
                        "comment": null,
                        "deleted_at": null,
                        "created_at": "2017-04-18 12:58:43",
                        "updated_at": "2017-04-18 12:58:43",
                        "profile": {
                            "name": "Dorris Jakubowski IV",
                            "weight": "109.21",
                            "height": "176.66",
                            "gender": "M",
                            "dob": "1993-08-28",
                            "biceps": "8.62",
                            "shoulders": "45.91",
                            "gym_name": "Adams-Smith",
                            "avatar": "uploads\/avatars\/M6pYujXszsU3axr6X4IvO73ZrrEF9S18BWPxqsCy.jpeg",
                            "ethnicity": 207623,
                            "latitude": "74.51276300",
                            "longitude": "-154.56846400",
                            "description": "WILL do next! As for pulling me out of the leaves: 'I should think you'll feel it a minute or two, she made out what it meant till now.' 'If that's all you know what they're like.' 'I believe so,'."
                        }
                    },
                    "defender_video": {
                        "id": 4,
                        "event_id": 2,
                        "user_id": 27,
                        "video_url": "http:\/\/gangster-strength.local.com",
                        "status": 1,
                        "uploader": 2,
                        "comment": null,
                        "deleted_at": null,
                        "created_at": "2017-04-19 07:15:18",
                        "updated_at": "2017-04-19 07:15:18",
                        "profile": {
                            "name": "Dorris Jakubowski IV",
                            "weight": "109.21",
                            "height": "176.66",
                            "gender": "M",
                            "dob": "1993-08-28",
                            "biceps": "8.62",
                            "shoulders": "45.91",
                            "gym_name": "Adams-Smith",
                            "avatar": "uploads\/avatars\/M6pYujXszsU3axr6X4IvO73ZrrEF9S18BWPxqsCy.jpeg",
                            "ethnicity": 207623,
                            "latitude": "74.51276300",
                            "longitude": "-154.56846400",
                            "description": "WILL do next! As for pulling me out of the leaves: 'I should think you'll feel it a minute or two, she made out what it meant till now.' 'If that's all you know what they're like.' 'I believe so,'."
                        }
                    }
                }
            }

+ Response 404 (application/json)
    + Body

            {
                "message": "No query results for model [App\\Models\\Challenge] 5",
                "status_code": 404
            }

## Like an Event/Challenge [POST /challenges/like/{id}]


+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "event": {
                    "id": 2,
                    "title": "Challenge Two",
                    "type": 1,
                    "status": 3,
                    "user_id": 27,
                    "description": "I am professioal Builder",
                    "created_at": "2017-04-18 12:58:42",
                    "tags": [
                        "100",
                        "Pushups"
                    ],
                    "likes": 2,
                    "liked": true,
                    "flagged": false
                }
            }

+ Response 404 (application/json)
    + Body

            {
                "message": "No query results for model [App\\Models\\Challenge] 5",
                "status_code": 404
            }

## Flag an Event/Challenge [POST /challenges/flag/{id}]


+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "event": {
                    "id": 2,
                    "title": "Challenge Two",
                    "type": 1,
                    "status": 3,
                    "user_id": 27,
                    "description": "I am professioal Builder",
                    "created_at": "2017-04-18 12:58:42",
                    "tags": [
                        "100",
                        "Pushups"
                    ],
                    "likes": 2,
                    "liked": true,
                    "flagged": false
                }
            }

+ Response 404 (application/json)
    + Body

            {
                "message": "No query results for model [App\\Models\\Challenge] 5",
                "status_code": 404
            }

# Friends [/friends]

## List of friends [GET /friends]


+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "total": 1,
                "per_page": 20,
                "current_page": 1,
                "last_page": 1,
                "next_page_url": null,
                "prev_page_url": null,
                "from": 1,
                "to": 1,
                "data": [
                    {
                        "id": 15,
                        "username": "trowe",
                        "email": "delphine52@example.com",
                        "created_at": "2017-04-06 06:06:41",
                        "profile": {
                            "name": "Dr. Billy White",
                            "weight": "65.61",
                            "height": "187.31",
                            "gender": "M",
                            "dob": "1977-09-15",
                            "biceps": "28.79",
                            "shoulders": "6.87",
                            "gym_name": "Vandervort, VonRueden and Krajcik",
                            "avatar": "http://lorempixel.com/640/480/?38925",
                            "ethnicity": 73063,
                            "latitude": "11.45609800",
                            "longitude": "-51.78216000",
                            "description": "Hic laborum consequatur quibusdam magni laborum iusto quod. Molestias optio molestias enim ducimus repellat suscipit vitae. Dolor non rerum earum et. Et quia et recusandae et ratione rerum."
                        }
                    }
                ]
            }

## Send friend request [POST /friends/send-request/{id}]


+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            []

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not send Friend request.",
                "errors": {
                    "user_id": [
                        "Already Send Request"
                    ]
                },
                "status_code": 422
            }

## Accept Friend Request [POST /friends/accept/{id}]


+ Response 200 (application/json)
    + Body

            []

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not accept Friend request.",
                "errors": {
                    "user_id": [
                        "Request doest not exist."
                    ]
                },
                "status_code": 422
            }

## Deny Friend Request [POST /friends/deny/{id}]


+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            []

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not deny Friend request.",
                "errors": {
                    "user_id": [
                        "Request doest not exist."
                    ]
                },
                "status_code": 422
            }

## Remove Friend [POST /friends/deny/{id}]


+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            []

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not remove Friend.",
                "errors": {
                    "user_id": [
                        "Request doest not exist."
                    ]
                },
                "status_code": 422
            }

## Block Friend [POST /friends/block/{id}]


+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            []

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not block Friend.",
                "errors": {
                    "user_id": [
                        "Friend doest not exist."
                    ]
                },
                "status_code": 422
            }

## Unblock Friend Request [POST /friends/unblock/{id}]


+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            []

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not unblock Friend.",
                "errors": {
                    "user_id": [
                        "Friend doest not exist."
                    ]
                },
                "status_code": 422
            }

# Devices [/devices]

## Register User [POST /devices/register]


+ Parameters
    + device_udid: (string, required) - Device UDID
    + firebase_token: (email, required) - Google firebase token

+ Request (application/json)
    + Body

            {
                "device_udid": "Device 2",
                "firebase_token": "token 3"
            }

+ Response 200 (application/json)
    + Body

            {
                "device": {
                    "device_udid": "Device 2",
                    "user_id": 15,
                    "firebase_token": "token 3",
                    "updated_at": "2017-04-27 09:50:33",
                    "created_at": "2017-04-27 09:50:33",
                    "id": 2
                }
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not register device.",
                "errors": {
                    "device_udid": [
                        "The device udid field is required."
                    ],
                    "firebase_token": [
                        "The firebase token field is required."
                    ]
                },
                "status_code": 422
            }

# Messages [/messages]

## List of Communications [GET /messages]


+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "message": {
                    "total": 2,
                    "per_page": 20,
                    "current_page": 1,
                    "last_page": 1,
                    "next_page_url": null,
                    "prev_page_url": null,
                    "from": 1,
                    "to": 2,
                    "data": [
                        {
                            "id": 1,
                            "last_message": {
                                "id": 52,
                                "thread_id": "1",
                                "sender_id": "4",
                                "body": "I will be in meeting room",
                                "created_at": "2017-05-09 11:14:00"
                            },
                            "messages": [
                                {
                                    "id": 52,
                                    "thread_id": "1",
                                    "sender_id": "4",
                                    "body": "I will be in meeting room",
                                    "created_at": "2017-05-09 11:14:00"
                                }
                            ],
                            "participants": [
                                {
                                    "id": 1,
                                    "thread_id": "1",
                                    "user_id": "4",
                                    "last_read": "2017-05-09 13:12:32",
                                    "deleted_at": null,
                                    "user": {
                                        "id": 4,
                                        "username": "jamaal23",
                                        "email": "jamaal23@example.org",
                                        "created_at": "2017-05-03 07:25:41",
                                        "banned_at": null,
                                        "profile": {
                                            "name": "jamaal 23",
                                            "weight": null,
                                            "height": null,
                                            "gender": null,
                                            "dob": null,
                                            "biceps": null,
                                            "shoulders": null,
                                            "gym_name": null,
                                            "avatar": null,
                                            "ethnicity": null,
                                            "latitude": null,
                                            "longitude": null,
                                            "description": null
                                        }
                                    }
                                },
                                {
                                    "id": 2,
                                    "thread_id": "1",
                                    "user_id": "11",
                                    "last_read": null,
                                    "deleted_at": null,
                                    "user": {
                                        "id": 11,
                                        "username": "cecelia.mertz",
                                        "email": "jacquelyn20@example.com",
                                        "created_at": "2017-05-09 10:05:19",
                                        "banned_at": null,
                                        "profile": {
                                            "name": "Roslyn Smitham",
                                            "weight": "140.14",
                                            "height": "126.77",
                                            "gender": "M",
                                            "dob": "1977-12-30",
                                            "biceps": "24.57",
                                            "shoulders": "42.74",
                                            "gym_name": "Glover, Lubowitz and Torphy",
                                            "avatar": "http:\/\/lorempixel.com\/640\/480\/?18089",
                                            "ethnicity": "4",
                                            "latitude": "73.93778600",
                                            "longitude": "-94.09201300",
                                            "description": "I'm grown up now,' she added in a hurry. 'No, I'll look first,' she said, 'for her hair goes in such a puzzled expression that she knew the meaning of it in asking riddles that have no idea what."
                                        }
                                    }
                                }
                            ],
                            "pivot": {
                                "user_id": "4",
                                "thread_id": "1",
                                "last_read": "2017-05-09 13:12:32"
                            }
                        }
                    ]
                }
            }

## Send Message to User [POST /messages]


+ Parameters
    + user_id: (integer, required) - 
    + body: (string, required) - 

+ Request (application/json)
    + Body

            {
                "user_id": 11,
                "body": "I will be in meeting room"
            }

+ Response 200 (application/json)
    + Body

            {
                "message": {
                    "id": 12,
                    "thread_id": "4",
                    "sender_id": "5",
                    "body": "I will be in meeting room",
                    "created_at": "2017-04-17 06:57:39"
                }
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not add Message.",
                "errors": {
                    "message": [
                        "The message field is required."
                    ],
                    "user_id": [
                        "The user id field is required."
                    ]
                },
                "status_code": 422
            }

+ Response 404 (application/json)
    + Body

            {
                "message": "No query results for model [App\\Models\\User] 411",
                "status_code": 404
            }

## List of Messages of Specific Thread [GET /messages/{id}]


+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "total": 52,
                "per_page": 3,
                "current_page": 1,
                "last_page": 18,
                "next_page_url": "http:\/\/localhost:8000\/api\/messages\/1?page=2",
                "prev_page_url": null,
                "from": 1,
                "to": 3,
                "data": [
                    {
                        "id": 52,
                        "thread_id": "1",
                        "sender_id": "4",
                        "body": "I will be in meeting room",
                        "created_at": "2017-05-09 11:14:00"
                    },
                    {
                        "id": 53,
                        "thread_id": "1",
                        "sender_id": "4",
                        "body": "I will be in meeting room",
                        "created_at": "2017-05-09 11:14:00"
                    },
                    {
                        "id": 49,
                        "thread_id": "1",
                        "sender_id": "4",
                        "body": "I will be in meeting room",
                        "created_at": "2017-05-09 11:13:59"
                    }
                ]
            }

## Unread Messages Count [GET /messages/unread-messages-count]


+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "unread_messages_count": 4
            }

## Read Thread/Messages [POST /messages/read-thread]


+ Parameters
    + thread_id: (integer, required) - 

+ Request (application/json)
    + Headers

            Authorization: Bearer {token}
    + Body

            {
                "thread_id": 2
            }

+ Response 200 (application/json)
    + Body

            {
                "message_thread": {
                    "id": 2,
                    "pivot": {
                        "user_id": "4",
                        "thread_id": "2",
                        "last_read": "2017-05-09 13:15:59"
                    }
                }
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not read Messages.",
                "errors": {
                    "thread_id": [
                        "The thread_id field is required."
                    ]
                },
                "status_code": 422
            }

# Sockets [/sockets]

## Register User [POST /sockets/register]


+ Parameters
    + socket_id: (string, required) - Socket ID

+ Request (application/json)
    + Body

            {
                "socket_id": "Socket 2"
            }

+ Response 200 (application/json)
    + Body

            {
                "socket": {
                    "socket_id": "Socket 2",
                    "user_id": 15,
                    "updated_at": "2017-04-27 09:50:33",
                    "created_at": "2017-04-27 09:50:33",
                    "id": 2
                }
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "Could not register socket.",
                "errors": {
                    "socket_id": [
                        "The socket udid field is required."
                    ]
                },
                "status_code": 422
            }

## Delete socket when connection break [POST /sockets/{socket_id}]


+ Parameters
    + socket_id: (string, required) - Socket ID

+ Response 200 (application/json)
    + Body

            {
                "socket": {
                    "socket_id": "Socket 2",
                    "user_id": 15,
                    "updated_at": "2017-04-27 09:50:33",
                    "created_at": "2017-04-27 09:50:33",
                    "id": 2
                }
            }