var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var redis = require('redis');

server.listen(8890);
io.on('connection', function (socket) {

//    console.log("client connected");
    console.log('client connected id: ' + socket.id);
    var redisClient = redis.createClient();
    redisClient.subscribe('message');
//    var msg = {message: socket.id};
//    msg = JSON.stringify(msg);
//    redisClient.set('message', msg);

    redisClient.on("message", function (channel, data) {
        data = JSON.parse(data);
        console.log("new message add in queue " + data.message + " channel: " + channel);
//        socket.broadcast.emit(channel, data);
        socket.broadcast.to(data.user).emit('message', data);
//        socket.emit(channel, data);
    });

    redisClient.on("notification", function (channel, data) {
        data = JSON.parse(data);
        console.log("new notification add in queue " + data.message + " channel: " + channel);
        socket.broadcast.to(data.user).emit('notification', data);
    });

    socket.on('disconnect', function () {
        var text = 'client disconnected id: ' + this.id;
        var publisher = redis.createClient();
        var obj = {text: text, socket_id: this.id};
//        publisher.publish("socket_disconnect", obj);
        publisher.publish("socket_disconnect", JSON.stringify(obj));
        redisClient.quit();
        publisher.quit();
    });

});