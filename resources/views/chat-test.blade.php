<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="https://cdn.socket.io/socket.io-1.3.4.js"></script>
<style type="text/css">
    #messages{
        border: 1px solid black;
        height: 300px;
        margin-bottom: 8px;
        overflow: scroll;
        padding: 5px;
    }
</style>
<div class="container spark-screen">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Chat Message Module</div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-lg-8" >
                            <div id="messages" ></div>
                        </div>
                        <div class="col-lg-8" >
                            <form action="sendmessage" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                                <textarea class="form-control msg"></textarea>
                                <input type="text" name="user" value="" >
                                <br/>
                                <input type="button" value="Send" class="btn btn-success send-msg">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
var socket = io.connect('http://localhost:8890');
socket.on('connect', function () {
    console.log(this.id)
});
socket.on('message', function (data) {
    console.log(data);
//    data = jQuery.parseJSON(data);
//    console.log(data.user);
//    $("#messages").append("<strong>" + data.user + ":</strong><p>" + data.message + "</p>");
});
socket.on('notification', function (data) {
    console.log(data);
//    data = jQuery.parseJSON(data);
//    console.log(data.user);
//    $("#messages").append("<strong>" + data.user + ":</strong><p>" + data.message + "</p>");
});
socket.on('disconnect', function () {
    var text = 'client disconnected id: ' + socket.id;
        console.log(text);
});
$(".send-msg").click(function (e) {
    e.preventDefault();
    var token = $("input[name='_token']").val();
    var user = $("input[name='user']").val();
    var msg = $(".msg").val();
    if (msg != '') {
//        console.log(msg)
//        socket.emit('message', {
//        message: msg
//    });
//    $(".msg").val('');
            $.ajax({
                type: "POST",
                url: '{!! URL::to("sendmessage") !!}',
                dataType: "json",
                data: {'_token':token,'message':msg,'user':user},
                success:function(data){
                    console.log(data);
                    $(".msg").val('');
                }
            });
    } else {
        alert("Please Add Message.");
    }
})
</script>